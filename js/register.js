(function($) {
    var firstNameValidation = "";
    var lastNameValidation = "";
    var emailValidation = "";
    var emailMatchValidation = "";
    var phoneValidation = "";
    var agreeRadius = ""
    var agreeRules = "";
    var captchaValidation = "";
    var supportNameValidation = "";
    var supportMessageValidation = "";
    var ageValidation = "";
    var dob_birthDay = "";
    var dob_birthMonth = "";
    var dob_birthYear = "";
    var address1 = "";
    var address2 = "";
    var cityValidation = "";
    var postalCode = "";
    var province = "";
    var phone = "";
    var message = "";
    var InvalidDate = "";
    var InvalidMonth = "";
    var InvalidYear = "";
    var termsValidation = "";
    var termsNotAccepted = "";
    var essayQuestion = "";

    var language = document.getElementsByTagName("html")[0].getAttribute("lang");
    if (language === "en") {
        firstNameValidation = "Please enter your first name.";
        lastNameValidation = "Please enter your last name.";
        emailValidation = "Please enter a valid email address.";
        emailMatchValidation = "Email does not match.";
        dob_birthDay = "Please select your birth date.";
        dob_birthMonth = "Please select your birth month.";
        dob_birthYear = "Please select your birth year.";
        address1 = "Please enter your first address.";
        address2 = "Please enter your second address.";
        phone = "Please enter your 10 digit number.";
        cityValidation = "Please enter your city.";
        postalCode = "Please enter your zip code.";
        province = "Please select your state.";
        phoneValidation = "Please provide your 10 digit number.";
        agreeRadius = "Please agree with the radius of the city";
        agreeRules = "Please agree with the terms and conditions of promotion";
        captchaValidation = "Please check the captcha checkbox.";
        supportNameValidation = "Please enter your name.";
        supportMessageValidation = "The message is required.";
        InvalidDate = "Please enter a valid date.";
        InvalidMonth = "Please enter a valid month.";
        InvalidYear = "Please enter a valid year.";
        ageValidation = "Either you have entered an Invalid date or you are below 18 years.";
        essayQuestion = "Please write 150 character essay.";
    } else {
        firstNameValidation = "Veuillez entrer votre prénom.";
        lastNameValidation = "Veuillez entrer votre nom de famille.";
        emailValidation = "Veuillez entrer une adresse électronique valide.";
        emailMatchValidation = "Les adresses électroniques ne correspondent pas.";
        dob_birthDay = "Veuillez sélectionner votre jour de naissance.";
        dob_birthMonth = "Veuillez sélectionner votre mois de naissance.";
        dob_birthYear = "Veuillez sélectionner votre année de naissance.";
        address1 = "Veuillez fournir votre première adresse.";
        address2 = "Please provide your second address.";
        postalCode = "Veuillez entrer votre code postal.";
        province = "Veuillez sélectionner votre province.";
        phoneValidation = "Veuillez fournir votre numéro à 10 chiffres.";
        agreeRadius = "Please agree with the radius of the city";
        agreeRules = "Please agree with the terms and conditions of promotion";
        captchaValidation = "Veuillez cocher la case captcha.";
        supportNameValidation = "Veuillez entrer votre nom.";
        supportMessageValidation = "Le message est obligatoire.";
        InvalidDate = "Veuillez entrer une date valide.";
        InvalidMonth = "Veuillez entrer une mois valide.";
        InvalidYear = "Veuillez entrer une année valide.";
        cityValidation = "Please enter your city FRENCH.";
        essayQuestion = "Please write 300 character essay.";
    }

    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validatePostalcode(postalCode) {
        if (postalCode.length == 5) {
            return true;
        } else {
            return false;
        }
    }

    function isagreeValidate() {
        var isAgreeCheck = document.getElementById("receiveEmail").checked;
        document.getElementById("receiveEmail").value = isAgreeCheck;
    }

    function formattedEmailValidation(email, confirmEmail) {
        var confirmedEmail = $("#" + confirmEmail);
        var formattedEmail = $("#" + email);
        if (formattedEmail.val() === "" || validateEmail(formattedEmail.val().trim()) === false || checkSpaces(formattedEmail.val())) {
            message = "<span class='help-block align-error'>" + emailValidation + "</span>";
            if (formattedEmail.next().html() != "") {
                formattedEmail.next().remove();
                formattedEmail.after(message);
            } else {
                formattedEmail.parent().addClass('has-error');
                formattedEmail.next().remove();
                formattedEmail.after(message);
            }
        } else {
            if (($("#" + confirmEmail)).val() != "" &&
                validateEmail(confirmedEmail.val().trim()) === true &&
                confirmedEmail.val().trim() != formattedEmail.val().trim()) {
                message = "<span class='help-block align-error'>" + emailMatchValidation + "</span>";
                if (confirmedEmail.next().html() != "" || confirmedEmail.next().next().html() != "") {
                    formattedEmail.next().remove();
                    confirmedEmail.next().next().remove();
                    confirmedEmail.next().remove();
                    confirmedEmail.after(message);
                } else {
                    formattedEmail.next().remove();
                    confirmedEmail.next().next().remove();
                    confirmedEmail.next().remove();
                    confirmedEmail.after(message);
                }
            } else {
                formattedEmail.next().remove();
            }
        }
    }

    function confirmEmailValidation(email, confirmEmail) {
        var confirmedEmail = $("#" + confirmEmail);
        var formattedEmail = $("#" + email);
        if ((confirmedEmail).val() === "" || validateEmail(confirmedEmail.val().trim()) === false) {
            message = "<span class='help-block align-error'>" + emailValidation + "</span>";
            if (confirmedEmail.next().html() != "") {
                confirmedEmail.next().remove();
                confirmedEmail.after(message);
            } else {
                confirmedEmail.parent().addClass('has-error');
                confirmedEmail.next().remove();
                confirmedEmail.after(message);
            }
        } else {
            if (confirmedEmail.val() != "" &&
                (validateEmail(confirmedEmail.val().trim())) === true &&
                (confirmedEmail.val().trim() != formattedEmail.val().trim())) {
                var message = "<span class='help-block align-error'>" + emailMatchValidation + "</span>";
                if (confirmedEmail.next().html() != "" || confirmedEmail.next().next().html() != "") {
                    formattedEmail.next().remove();
                    confirmedEmail.next().next().remove();
                    confirmedEmail.next().remove();
                    confirmedEmail.after(message);
                } else {
                    formattedEmail.next().remove();
                    confirmedEmail.next().remove();
                    confirmedEmail.next().next().remove();
                    confirmedEmail.after(message);
                }
            } else {
                formattedEmail.next().remove();
                confirmedEmail.next().remove();
            }
        }
    }

    $(document).ready(function() {
        var shouldSubmit = true;

        var elementExists = document.getElementById("user_essayQuestion");

        $("#user_essayQuestion").on('blur', function() {
            var str = $(this).val();
            if (str.length > 300 || str.length === 0) {
                var message = "<span class='help-block align-error'>" + essayQuestion + "  </span>";
                $("#user_essayQuestion").parent().append($("#user_essayQuestion").parent().find("> span").html(essayQuestion));
            } else {
                $("#user_essayQuestion").next().html("");
            }
        });

        $("#firstName").on('blur', function() {
            var name = $("#firstName");
            if (name.val() === "" || checkSpaces(name.val())) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + firstNameValidation + "</span>";
                if (name.next().html() != "") {
                    name.next().remove();
                    name.after(message);
                } else {
                    name.next().remove();
                    name.after(message);
                }
            } else {
                shouldSubmit = true;
                name.next().remove();
            }
        });
        $("#name").on('blur', function() {
            if (($("#name")).val() === "" || checkSpaces($("#name").val())) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + supportNameValidation + "</span>";
                if ($("#name").next().html() != "") {
                    $("#name").next().remove();
                    $("#name").after(message);
                } else {
                    $("#name").next().remove();
                    $("#name").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#name").next().remove();
            }
        });
        $("#email").on('blur', function() {
            if (($("#email")).val() === "" || checkSpaces($("#name").val()) || validateEmail($("#email").val().trim()) === false) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + emailValidation + "</span>";
                if ($("#email").next().html() != "") {
                    $("#email").next().remove();
                    $("#email").after(message);
                } else {
                    $("#email").next().remove();
                    $("#email").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#email").next().remove();
            }
        });

        $("#message").on('blur', function() {
            if (($("#message")).val() === "" || checkSpaces($("#message").val())) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + supportMessageValidation + "</span>";
                if ($("#message").next().html() != "") {
                    $("#message").next().remove();
                    $("#message").after(message);
                } else {
                    $("#message").next().remove();
                    $("#message").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#message").next().remove();
            }
        });

        $("#lastName").on('blur', function() {
            if (($("#lastName")).val() === "" || checkSpaces($("#lastName").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + lastNameValidation + "</span>";
                if ($("#lastName").next().html() != "") {
                    $("#lastName").next().remove();
                    $("#lastName").after(message);
                } else {
                    $("#lastName").parent().addClass('has-error');
                    $("#lastName").next().remove();
                    $("#lastName").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#lastName").next().html("");
            }
        });

        $("#emailGroup_email").on('blur', function() {
            formattedEmailValidation("emailGroup_email", 'emailGroup_confirmedEmail')
        });

        $("#emailGroup_confirmedEmail").on('blur', function() {
            confirmEmailValidation("emailGroup_email", 'emailGroup_confirmedEmail')
        });

        $("#ageAndEmail_ageRange").on('blur', function() {

            if ($("#ageAndEmail_ageRange").val() === "17") {
                $("#ageAndEmail_parentEmailGroup_email").on('blur', function() {
                    if ($("#ageAndEmail_ageRange").val() === "17") {
                        formattedEmailValidation("ageAndEmail_parentEmailGroup_email", 'ageAndEmail_parentEmailGroup_confirmedEmail')
                    }
                });

                $("#ageAndEmail_parentEmailGroup_confirmedEmail").on('blur', function() {
                    if ($("#ageAndEmail_ageRange").val() === "17") {
                        confirmEmailValidation("ageAndEmail_parentEmailGroup_email", 'ageAndEmail_parentEmailGroup_confirmedEmail')
                    }
                });
            } else if ($("#ageAndEmail_ageRange").val() === "18" || $('#ageAndEmail_ageRange option:nth(0)').val() === "") {
                $("#ageAndEmail_parentEmailGroup_email").next().remove();
                $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                $("#ageAndEmail_parentEmailGroup_email").on('blur', function() {
                    parentEmailValidate();
                });
                $("#ageAndEmail_parentEmailGroup_confirmedEmail").on('blur', function() {
                    parentConfirmEmailValidate();
                });
            }
        });

        $("#ageAndEmail_parentEmailGroup_email").on('blur', function() {
            parentEmailValidate();
        });

        function parentEmailValidate() {
            if (($("#ageAndEmail_parentEmailGroup_email")).val() != "") {
                if (validateEmail($("#ageAndEmail_parentEmailGroup_email").val().trim()) === false ||
                    checkSpaces($("#ageAndEmail_parentEmailGroup_email").val())) {
                    shouldSubmit = false;
                    var message = "<span class='help-block align-error'>" + emailValidation + "</span>";
                    if ($("#ageAndEmail_parentEmailGroup_email").next().html() != "") {
                        $("#ageAndEmail_parentEmailGroup_email").next().remove();
                        $("#ageAndEmail_parentEmailGroup_email").after(message);
                    } else {
                        $("#ageAndEmail_parentEmailGroup_email").parent().addClass('has-error');
                        $("#ageAndEmail_parentEmailGroup_email").next().remove();
                        $("#ageAndEmail_parentEmailGroup_email").after(message);
                    }
                } else {
                    if (($("#ageAndEmail_parentEmailGroup_confirmedEmail")).val() != "" &&
                        validateEmail($("#ageAndEmail_parentEmailGroup_confirmedEmail").val().trim()) === true &&
                        ($("#ageAndEmail_parentEmailGroup_confirmedEmail")).val().trim() != ($("#ageAndEmail_parentEmailGroup_email")).val().trim()) {
                        shouldSubmit = false;
                        var message = "<span class='help-block align-error'>" + emailMatchValidation + "</span>";
                        if ($("#ageAndEmail_parentEmailGroup_confirmedEmail").next().html() != "" ||
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().html() != "") {
                            $("#ageAndEmail_parentEmailGroup_email").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                        } else {
                            $("#ageAndEmail_parentEmailGroup_email").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                        }
                    } else {
                        shouldSubmit = true;
                        $("#ageAndEmail_parentEmailGroup_email").next().remove();
                    }
                }
            }
        }

        $("#ageAndEmail_parentEmailGroup_confirmedEmail").on('blur', function() {
            parentConfirmEmailValidate();
        });

        function parentConfirmEmailValidate() {
            if (($("#ageAndEmail_parentEmailGroup_confirmedEmail")).val() != "") {
                if (validateEmail($("#ageAndEmail_parentEmailGroup_confirmedEmail").val().trim()) === false) {
                    shouldSubmit = false;
                    var message = "<span class='help-block align-error'>" + emailValidation + "</span>";
                    if ($("#ageAndEmail_parentEmailGroup_confirmedEmail").next().html() != "") {
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                    } else {
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").parent().addClass('has-error');
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                    }
                } else {
                    if (($("#ageAndEmail_parentEmailGroup_confirmedEmail")).val() != "" &&
                        validateEmail($("#ageAndEmail_parentEmailGroup_confirmedEmail").val().trim()) === true &&
                        ($("#ageAndEmail_parentEmailGroup_confirmedEmail")).val().trim() != ($("#ageAndEmail_parentEmailGroup_email")).val().trim()) {
                        shouldSubmit = false;
                        var message = "<span class='help-block align-error'>" + emailMatchValidation + "</span>";
                        if ($("#ageAndEmail_parentEmailGroup_confirmedEmail").next().html() != "" || $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().html() != "") {
                            $("#ageAndEmail_parentEmailGroup_email").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                        } else {
                            $("#ageAndEmail_parentEmailGroup_email").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().next().remove();
                            $("#ageAndEmail_parentEmailGroup_confirmedEmail").after(message);
                        }
                    } else {
                        shouldSubmit = true;
                        $("#ageAndEmail_parentEmailGroup_email").next().remove();
                        $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
                    }
                }
            }
        }
        $("#city").on('blur', function() {
            if (($("#city")).val() === "" || checkSpaces($("#city").val())) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + cityValidation + "</span>";
                if ($("#city").next().html() != "") {
                    $("#city").next().remove();
                    $("#city").after(message);
                } else {
                    $("#city").next().remove();
                    $("#city").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#city").next().remove();
            }
        });
        $("#province").on('blur', function() {
            var province = $("#province").val();
            if (province == "") {
                var message = "<span class='help-block align-error'>Please select your state.</span>";
                if ($("#province").next().html() != "") {
                    $("#province").next().remove();
                    $("#province").after(message);
                } else {
                    $("#province").parent().addClass('has-error');
                    $("#province").next().remove();
                    $("#province").after(message);
                }
            } else {
                $("#province").next().html("");
            }
        });
        $("#phone").on('blur', function() {
            if (($("#phone")).val().length != 10 || checkSpaces($("#phone").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + phoneValidation + "</span>";
                if ($("#phone").next().html() != "") {
                    $("#phone").next().remove();
                    $("#phone").after(message);
                } else {
                    $("#phone").parent().addClass('has-error');
                    $("#phone").next().remove();
                    $("#phone").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#phone").next().html("");
            }
        });
        $("#ageAndEmail_ageRange").on('blur', function() {
            var ageRange = $("#ageAndEmail_ageRange").val();
            if (ageRange == "") {
                var message = "<span class='help-block align-error'>Please select your age range.</span>";
                if ($("#ageAndEmail_ageRange").next().html() != "") {
                    $("#ageAndEmail_ageRange").next().remove();
                    $("#ageAndEmail_ageRange").after(message);
                } else {
                    $("#ageAndEmail_ageRange").parent().addClass('has-error');
                    $("#ageAndEmail_ageRange").next().remove();
                    $("#ageAndEmail_ageRange").after(message);
                }
            } else {
                $("#ageAndEmail_ageRange").next().html("");
            }
        });

        function validatedate() {
            var day = $("#dob_birthDay").val();
            var month = $("#dob_birthMonth").val();
            var year = $("#dob_birthYear").val();
            if (month && day && year) {
                var inputText = month + '/' + day + '/' + year;
                var dateformat = /^(?:(?:(?:0?[13578]|1[02])(\/|-|\.)31)\1|(?:(?:0?[1,3-9]|1[0-2])(\/|-|\.)(?:29|30)\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:0?2(\/|-|\.)29\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:(?:0?[1-9])|(?:1[0-2]))(\/|-|\.)(?:0?[1-9]|1\d|2[0-8])\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/;
                // Match the date format through regular expression
                if (inputText.match(dateformat)) {
                    $("#dob_birthDay").next().html("");
                    $("#dob_birthDay").next().next().html("");
                    $("#dob_birthMonth").next().html("");
                    $("#dob_birthMonth").next().next().html("");
                    $("#dob_birthYear").next().html("");
                    $("#dob_birthYear").next().next().html("");
                } else {
                    var message = "<span class='help-block align-error'>" + InvalidDate + "</span>";
                    var monthMessage = "<span class='help-block align-error'>" + InvalidMonth + "</span>";
                    var yearMessage = "<span class='help-block align-error'>" + InvalidYear + "</span>";
                    if ($("#dob_birthDay").next().html() != "" || $("#dob_birthMonth").next().html() != "" || $("#dob_birthYear").next().html() != "") {
                        $("#dob_birthDay").next().remove();
                        $("#dob_birthDay").after(message);
                        $("#dob_birthMonth").next().remove();
                        $("#dob_birthMonth").after(monthMessage);
                        $("#dob_birthYear").next().remove();
                        $("#dob_birthYear").after(yearMessage);
                    } else {
                        $("#dob_birthDay").parent().addClass('has-error');
                        $("#dob_birthDay").next().remove();
                        $("#dob_birthDay").after(message);
                        $("#dob_birthMonth").next().remove();
                        $("#dob_birthMonth").after(monthMessage);
                        $("#dob_birthYear").next().remove();
                        $("#dob_birthYear").after(yearMessage);
                    }
                }
            }
        }

        $("#dob_birthDay").on('blur', function() {

            if (($("#dob_birthDay")).val() === "" || checkSpaces($("#dob_birthDay").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + dob_birthDay + "</span>";
                if ($("#dob_birthDay").next().html() != "") {
                    $("#dob_birthDay").next().remove();
                    $("#dob_birthDay").after(message);
                } else {
                    $("#dob_birthDay").parent().addClass('has-error');
                    $("#dob_birthDay").next().remove();
                    $("#dob_birthDay").after(message);
                }
            } else {
                $("#dob_birthDay").next().html("");
                $("#dob_birthDay").next().next().html("");
                /*checkLeap();*/
                validatedate();
            }
        });
        $("#dob_birthMonth").on('blur', function() {
            if (($("#dob_birthMonth")).val() === "" || checkSpaces($("#dob_birthMonth").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + dob_birthMonth + "</span>";
                if ($("#dob_birthMonth").next().html() != "") {
                    $("#dob_birthMonth").next().remove();
                    $("#dob_birthMonth").after(message);
                } else {
                    $("#dob_birthMonth").parent().addClass('has-error');
                    $("#dob_birthMonth").next().remove();
                    $("#dob_birthMonth").after(message);
                }
            } else {
                $("#dob_birthMonth").next().html("");
                $("#dob_birthMonth").next().next().html("");
                /*checkLeap();*/
                validatedate();
            }
        });
        $("#dob_birthYear").on('blur', function() {

            if (($("#dob_birthYear")).val() === "" || checkSpaces($("#dob_birthYear").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + dob_birthYear + "</span>";
                if ($("#dob_birthYear").next().html() != "") {
                    $("#dob_birthYear").next().remove();
                    $("#dob_birthYear").after(message);
                } else {
                    $("#dob_birthYear").parent().addClass('has-error');
                    $("#dob_birthYear").next().remove();
                    $("#dob_birthYear").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#dob_birthYear").next().html("");
                $("#dob_birthYear").next().next().html("");
                validatedate();
            }
        });

        $("#address1").on('blur', function() {
            if (($("#address1")).val() === "" || checkSpaces($("#address1").val())) {
                shouldSubmit = false;
                var message = "<span class='help-block align-error'>" + address1 + "</span>";
                if ($("#address1").next().html() != "") {
                    $("#address1").next().remove();
                    $("#address1").after(message);
                } else {
                    $("#address1").parent().addClass('has-error');
                    $("#address1").next().remove();
                    $("#address1").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#address1").next().html("");
            }
        });

        $("#postalCode").on('blur', function() {
            if (($("#postalCode")).val() === "" || checkSpaces($("#postalCode").val()) || validatePostalcode($("#postalCode").val().trim()) === false) {
                shouldSubmit = false;

                var message = "<span class='help-block align-error'>" + postalCode + "</span>";
                if ($("#postalCode").next().html() != "") {
                    $("#postalCode").next().remove();
                    $("#postalCode").after(message);
                } else {
                    $("#postalCode").next().remove();
                    $("#postalCode").after(message);
                }
            } else {
                shouldSubmit = true;
                $("#postalCode").next().html("");
            }
        });

        $("#isAgreeArea").on('change', function() {
            var message = "<span class='help-block red-txt align-error'>" + agreeRadius + "</span>";
            if (!$('#isAgreeArea').is(':checked')) {
                shouldSubmit = false;
                $('.checkbox').next().text('');
                //$("#isAgreeTerm").checked = true;

                $('#isAgreeArea').parent().parent().append(message);
            } else {
                shouldSubmit = true;
                $('#isAgreeArea').parent().parent().find('.align-error').html("");

            }
        });

        $("#isAgree").on('change', function() {
            var message = "<span class='help-block red-txt align-error'>" + agreeRules + "</span>";
            if (!$('#isAgree').is(':checked')) {
                shouldSubmit = false;
                $('.checkbox').next().text('');
                //$("#isAgreeTerm").checked = true;

                $('#isAgree').parent().parent().append(message);
            } else {
                shouldSubmit = true;
                $('#isAgree').parent().parent().find('.align-error').html("");

            }
        });
    });

    function secOffset(fff) {
        $('html,body').animate({
            'scrollTop': $('span.align-error').parent().offset().top
        }, 200);
    }

    function checkSpaces(str) {
        if ((jQuery.trim(str)).length == 0) {
            return true;
        } else {
            // return true;
        }
    }

    function onloadCallback() {
        var elementExists = document.getElementById("user_essayQuestion");
        if (($("#firstName")).val() === "" || checkSpaces($("#firstName").val())) {
            $("#firstName").parent().append($("#firstName").parent().find("> span").html(firstNameValidation));
            secOffset("#firstName");
        }
        if (($("#lastName")).val() === "" || checkSpaces($("#lastName").val())) {
            if ($("#lastName").next().html() === "") {
                $("#lastName").parent().append($("#lastName").parent().find("> span").html(lastNameValidation));
            }
            secOffset("#lastName");
        }
        if (($("#emailGroup_email")).val() === "" || validateEmail($("#emailGroup_email").val().trim()) === false) {
            $("#emailGroup_email").parent().find(">span:first").html(emailValidation);
        } else {
            if (($("#emailGroup_confirmedEmail")).val() != "" &&
                validateEmail($("#emailGroup_confirmedEmail").val().trim()) === true &&
                ($("#emailGroup_confirmedEmail")).val().trim() != ($("#emailGroup_email")).val().trim()) {
                if ($("#emailGroup_confirmedEmail").next().html() === "") {

                    $("#emailGroup_confirmedEmail").parent().append("<span class='help-block align-error'>" + emailMatchValidation + "</span>");
                }
                $("#emailGroup_confirmedEmail").parent().find("> span:first").html(emailMatchValidation);
            }
        }
        if (($("#emailGroup_confirmedEmail")).val() === "" || validateEmail($("#emailGroup_confirmedEmail").val().trim()) === false) {
            if ($("#emailGroup_confirmedEmail").next().html() === "") {
                $("#emailGroup_confirmedEmail").parent().append("<span class='help-block align-error matchVal'>" + emailMatchValidation + "</span>");
            }
            if ($("#emailGroup_confirmedEmail").parent().find("> span").length > 1) {
                $('.matchVal').remove();
                $("#emailGroup_confirmedEmail").parent().find("> span:first").html(emailValidation);
            }
            secOffset("#emailGroup_confirmedEmail");
        } else {
            if (($("#emailGroup_confirmedEmail")).val().trim() != ($("#emailGroup_email")).val().trim()) {
                $("#emailGroup_confirmedEmail").parent().find("> span:first").html(emailMatchValidation);
                secOffset("#emailGroup_confirmedEmail");
            }
        }
        if (($("#dob_birthDay")).val() === "" || checkSpaces($("#dob_birthDay").val())) {
            $("#dob_birthDay").parent().append($("#dob_birthDay").parent().find("> span").html(dob_birthDay));
        }
        if (($("#dob_birthMonth")).val() === "" || checkSpaces($("#dob_birthMonth").val())) {
            $("#dob_birthMonth").parent().append($("#dob_birthMonth").parent().find("> span").html(dob_birthMonth));
        }
        if (($("#dob_birthYear")).val() === "" || checkSpaces($("#dob_birthYear").val())) {
            $("#dob_birthYear").parent().append($("#dob_birthYear").parent().find("> span").html(dob_birthYear));
        }
        if (($("#address1")).val() === "" || checkSpaces($("#address1").val())) {
            $("#address1").parent().append($("#address1").parent().find("> span").html(address1));
        }
        if (($("#city")).val() === "" || checkSpaces($("#city").val())) {
            $("#city").parent().append($("#city").parent().find("> span").html(cityValidation));
        }
        if (($("#postalCode")).val() === "" || checkSpaces($("#postalCode").val())) {
            $("#postalCode").parent().append($("#postalCode").parent().find("> span").html(postalCode));
        }
        if (($("#province")).val() === "" || checkSpaces($("#province").val())) {
            $("#province").parent().append($("#province").parent().find("> span").html(province));
        }
        if (($("#phone")).val() === "" || checkSpaces($("#phone").val())) {
            $("#phone").parent().append($("#phone").parent().find("> span").html(phoneValidation));
        }
        if (elementExists != null) {
            if (($("#user_essayQuestion")).val() === "" || checkSpaces($("#user_essayQuestion").val())) {
                $("#user_essayQuestion").parent().append($("#user_essayQuestion").parent().find("> span").html(essayQuestion));

            }
        }

        /*if ($("#ageAndEmail_ageRange").val() === "") {
         $("#ageAndEmail_ageRange").parent().append($("#ageAndEmail_ageRange").parent().find("> span").html("Please select your age range."));
         } else {
         if ($("#ageAndEmail_ageRange").val() === "17") {
         formattedEmailValidation("ageAndEmail_parentEmailGroup_email", 'ageAndEmail_parentEmailGroup_confirmedEmail');
         confirmEmailValidation("ageAndEmail_parentEmailGroup_email", 'ageAndEmail_parentEmailGroup_confirmedEmail')
         } else {
         if ($("#ageAndEmail_ageRange").val() === "18") {
         $("#ageAndEmail_parentEmailGroup_email").next().remove();
         $("#ageAndEmail_parentEmailGroup_confirmedEmail").next().remove();
         }
         }
         }*/
        if (!$('#isAgreeArea').is(':checked')) {
            var message = "<span class='help-block red-txt align-error'>" + agreeRadius + "</span>";
            shouldSubmit = false;
            $('.checkbox').next().text('');
            //$("#isAgreeTerm").checked = true;

            $('#isAgreeArea').parent().parent().append(message);
        } else {
            shouldSubmit = true;
            $('#isAgreeArea').parent().parent().find('.align-error').html("");

        }

        if (!$('#isAgree').is(':checked')) {
            var message = "<span class='help-block red-txt align-error'>" + agreeRules + "</span>";
            shouldSubmit = false;
            $('.checkbox').next().text('');
            //$("#isAgreeTerm").checked = true;

            $('#isAgree').parent().parent().append(message);
        } else {
            shouldSubmit = true;
            $('#isAgree').parent().parent().find('.align-error').html("");

        }

        var captcha_response = grecaptcha.getResponse();
        if (captcha_response.length === 0) {
            $("#captcha").parent().find(">span").html(captchaValidation);
        }

        var errorSpan = $("span.align-error");
        for (var k = 0; k < errorSpan.length; k++) {
            console.log("inside for loop");
            if (errorSpan[k].innerHTML.length > 0) {
                return false;
            }
        }
    }

    function onModalClick() {
        if (!$('#terms').is(':checked')) {
            shouldSubmit = false;
            $('.checkbox').next().text('');

            $('#terms').parent().find(">span.align-error:first").html(termsValidation);
            secOffset("#terms");
            if ($('#noTerms').is(':checked')) {
                $('#noTerms').parent().parent().find('.align-error').html("");
                $('#noTerms').attr('checked', false);
            }
            // return false;
        } else {
            shouldSubmit = true;
            $('#terms').parent().parent().find('.align-error').html("");

        }
        var errorSpan = $("span.align-error");
        for (var k = 0; k < errorSpan.length; k++) {
            if (errorSpan[k].innerHTML.length > 0) {
                return false;
            } else {}
        }
    }

    function onCloseClick() {

        var errorSpan = $("span.align-error");
        for (var k = 0; k < errorSpan.length; k++) {
            if (errorSpan[k].innerHTML.length > 0) {
                $('span.align-error').parent().parent().find('.align-error').html("");
            } else {}
        }
    }

    $("#submitForm").click(onloadCallback);
    $("#modalForm").click(onModalClick);
    $("#modalClose").click(onCloseClick);

    function onSupportCallback() {
        if (($("#name")).val() === "" || checkSpaces($("#name").val())) {
            $("#name").parent().append($("#name").parent().find("> span").html(supportNameValidation));
            secOffset("#firstName");
        }
        if (($("#email")).val() === "" || checkSpaces($("#email").val())) {
            $("#email").parent().append($("#email").parent().find("> span").html(emailValidation));
            secOffset("#firstName");
        }
        if (($("#message")).val() === "" || checkSpaces($("#message").val())) {
            $("#message").parent().append($("#message").parent().find("> span").html(supportMessageValidation));
            secOffset("#firstName");
        }

        var errorSpan = $("span.align-error");
        for (var k = 0; k < errorSpan.length; k++) {
            if (errorSpan[k].innerHTML.length > 0) {
                return false;
            } else {}
        }
    }

    $("#supportForm").click(onSupportCallback);

    var ownerAdress = "";

    ownerAdress = "<p>Mars Canada Inc.<br> P.O. Box / C.P. 640<br> Bolton, Ontario L7E 5S4<br> Consumer Affairs 1-888-709-6277</p>";

    function siteOwner() {
        swal({ html: true, title: '', text: '<b>' + ownerAdress + '</b>' });
    }

    $("#site").click(siteOwner);
})
(jQuery);